use crate::Config;
use axum::{
    extract::Query,
    http::{header, StatusCode},
    response::IntoResponse,
    Extension, Json,
};
use chrono::Utc;
use csv::WriterBuilder;
use gw2lib::{
    model::{
        authenticated::{
            account::{bank::Bank, materials::AccountMaterials, wallet::Wallet, Account},
            characters::Character,
        },
        BulkEndpoint,
    },
    rate_limit::BucketRateLimiter,
    Client, Requester,
};
use serde::{Deserialize, Serialize};
use sqlx::sqlite::{SqliteConnectOptions, SqlitePoolOptions};
use sqlx::types::Json as Json2;
use sqlx::{Error, Pool, Sqlite};
use std::fmt::Write as _;
use std::str::FromStr;
use std::{
    collections::{BTreeMap, HashMap},
    sync::Arc,
};
use tokio::sync::Mutex;

pub async fn db_init(database: &str) -> Result<Pool<Sqlite>, Error> {
    let pool = SqlitePoolOptions::new().max_connections(5).connect_with(SqliteConnectOptions::from_str(&format!("sqlite://{}", database))?.create_if_missing(true)).await?;

    sqlx::query(
        "CREATE TABLE IF NOT EXISTS free_bag_slots (
            account text not null,
            last_update text not null,
            name text primary key,
            total integer not null,
            used integer not null,
            free integer not null
         )",
    )
    .execute(&pool)
    .await?;

    sqlx::query(
        "CREATE TABLE IF NOT EXISTS login_rewards (
            id text primary key,
            display_name text not null,
            last_update text not null,
            wallet_laurel integer not null,
            items text not null
         )",
    )
    .execute(&pool)
    .await?;

    // for api keys information
    sqlx::query(
        "CREATE TABLE IF NOT EXISTS account_data (
            key text primary key,
            account text not null,
            characters text not null
         )",
    )
    .execute(&pool)
    .await?;

    // set up indexes?
    //sqlx::query("CREATE INDEX IF NOT EXISTS index_estimate ON bus_results (valid_estimate)").execute(&pool).await?;

    Ok(pool)
}

#[derive(Debug, Deserialize, Serialize, sqlx::FromRow)]
struct AccountData {
    key: String,
    account: Json2<Account>,
    characters: Json2<Vec<Character>>,
}

// this is only to fetch
pub async fn get_account_data(config: &Config, rate_limiter: &Arc<Mutex<BucketRateLimiter>>, connection: &Pool<Sqlite>) {
    for key in &config.keys {
        let client = Client::default().rate_limiter(rate_limiter.clone()).api_key(key).await;
        let account: Account = client.get().await.unwrap();
        let characters: Vec<Character> = client.all().await.unwrap();

        let entry = AccountData { key: key.clone(), account: Json2(account), characters: Json2(characters) };

        sqlx::query_as::<_, AccountData>(
            r#"
                INSERT OR REPLACE INTO account_data (key, account, characters)
                VALUES (?1, ?2, ?3)
                "#,
        )
        .bind(&entry.key)
        .bind(Json2(&entry.account))
        .bind(Json2(&entry.characters))
        .fetch_optional(connection)
        .await
        .ok();
    }
}

#[derive(Debug, Deserialize, Serialize, sqlx::FromRow)]
struct ResultBags {
    account: String,
    name: String,
    last_update: String,
    total: u16,
    used: u16,
    free: u16,
}

// this is only to fetch
pub async fn get_free_bag_slots(config: &Config, connection: &Pool<Sqlite>) {
    for key in &config.keys {
        let account_data = sqlx::query_as::<_, AccountData>("SELECT * FROM account_data WHERE key == ?").bind(&key).fetch_one(connection).await.unwrap();

        // gotta get it out of Json2(T)
        let account = account_data.account.as_ref();
        let characters = account_data.characters.as_ref();

        for character in characters {
            let mut total = 0;
            let mut used = 0;

            for bag_option in character.bags.iter().flatten() {
                total += bag_option.size as u16;

                for slot in &bag_option.inventory {
                    match slot {
                        None => {}
                        Some(_) => {
                            used += 1;
                        }
                    }
                }
            }
            let result = ResultBags { account: account.name.clone(), last_update: format!("{:?}", Utc::now()), name: character.id().clone(), total, used, free: total - used };

            sqlx::query_as::<_, ResultBags>(
                r#"
                INSERT OR REPLACE INTO free_bag_slots (account, name, last_update, total, used, free)
                VALUES (?1, ?2, ?3, ?4, ?5, ?6)
                "#,
            )
            .bind(&result.account)
            .bind(&result.name)
            .bind(&result.last_update)
            .bind(&result.total)
            .bind(&result.used)
            .bind(&result.free)
            .fetch_optional(connection)
            .await
            .ok();
        }
    }
}

async fn get_free_bag_slots_db(config: &Config, connection: &Pool<Sqlite>) -> Vec<ResultBags> {
    let mut results = vec![];
    for key in &config.keys {
        let account_data = sqlx::query_as::<_, AccountData>("SELECT * FROM account_data WHERE key == ?").bind(&key).fetch_one(connection).await.unwrap();

        // gotta get it out of Json2(T)
        let account = account_data.account.as_ref();

        let database_results = sqlx::query_as::<_, ResultBags>("SELECT * FROM free_bag_slots WHERE account == ?").bind(&account.name).fetch_all(connection).await.unwrap();

        results.extend(database_results);
    }
    results
}

pub async fn free_bag_slots(Extension(config): Extension<Config>, Extension(connection): Extension<Pool<Sqlite>>) -> impl IntoResponse {
    let results = get_free_bag_slots_db(&config, &connection).await;
    (StatusCode::OK, [(header::CONTENT_TYPE, "application/json; charset=utf-8")], Json(results))
}

pub async fn free_bag_slots_csv(Query(params): Query<HashMap<String, String>>, Extension(config): Extension<Config>, Extension(connection): Extension<Pool<Sqlite>>) -> impl IntoResponse {
    let delimiter = get_delimiter(&params);

    let results = get_free_bag_slots_db(&config, &connection).await;
    let mut wtr = WriterBuilder::new().delimiter(delimiter as u8).quote_style(csv::QuoteStyle::NonNumeric).from_writer(vec![]);
    for result in results {
        wtr.serialize(result).unwrap();
    }
    let data = String::from_utf8(wtr.into_inner().unwrap()).unwrap();

    (StatusCode::OK, [(header::CONTENT_TYPE, "text/csv; charset=utf-8"), (header::CONTENT_DISPOSITION, "attachment; filename=free_bag_slots.csv")], data)
}

#[derive(Debug, Deserialize, Serialize, sqlx::FromRow)]
struct LoginRewardsItems {
    id: String,
    display_name: String,
    last_update: String,
    wallet_laurel: u32,

    #[serde(flatten)]
    items: Json2<BTreeMap<u32, u32>>,
}

pub async fn get_login_rewards(config: &Config, rate_limiter: &Arc<Mutex<BucketRateLimiter>>, connection: &Pool<Sqlite>) {
    // numeric version of the items, add new items on teh end
    let items = [19976, 43766, 68314, 68315, 68316, 68317, 68318, 68319, 68320, 68321, 68322, 68323, 68324, 68325, 68327, 68328, 68329, 68330, 68331, 68332, 68333, 68334, 68335, 68336, 68337, 68338, 68339, 68340, 68341, 68350, 68351, 68352, 68354, 68326];

    for key in &config.keys {
        let client = Client::default().rate_limiter(rate_limiter.clone()).api_key(key).await;

        // pull up the cached results
        let account_data = sqlx::query_as::<_, AccountData>("SELECT * FROM account_data WHERE key == ?").bind(&key).fetch_one(connection).await.unwrap();
        let account = account_data.account.as_ref();

        let mut result = LoginRewardsItems {
            id: account.id.clone(),
            display_name: account.name.clone(),
            last_update: "".to_string(),
            wallet_laurel: 0,
            // store the items this way, easy to extend by adding numbers above
            items: Json2(BTreeMap::from(items.map(|c| (c, 0_u32)))),
        };

        let characters = account_data.characters.as_ref();
        for character in characters {
            for bag_option in character.bags.iter().flatten() {
                for item in bag_option.inventory.iter().flatten() {
                    if let Some(value) = result.items.get_mut(&item.id) {
                        *value += 1;
                    }
                }
            }
        }

        // bank
        let bank: Bank = client.get().await.unwrap();
        for item in bank.into_iter().flatten() {
            if let Some(value) = result.items.get_mut(&item.id) {
                *value += 1;
            }
        }

        // mat storage
        let mat: AccountMaterials = client.get().await.unwrap();
        for item in mat {
            if let Some(value) = result.items.get_mut(&item.id) {
                *value += 1;
            }
        }

        // wallet
        let wallet: Wallet = client.get().await.unwrap();
        if let Some(value) = wallet.get(&3) {
            result.wallet_laurel = *value
        }

        result.last_update = format!("{:?}", Utc::now());

        // save it
        sqlx::query_as::<_, LoginRewardsItems>(
            r#"
                INSERT OR REPLACE INTO login_rewards (id, display_name, last_update, wallet_laurel, items)
                VALUES (?1, ?2, ?3, ?4, ?5)
                "#,
        )
        .bind(&result.id)
        .bind(&result.display_name)
        .bind(&result.last_update)
        .bind(&result.wallet_laurel)
        .bind(Json2(&result.items))
        .fetch_optional(connection)
        .await
        .ok();
    }
}

async fn get_login_rewards_db(config: &Config, connection: &Pool<Sqlite>) -> Vec<LoginRewardsItems> {
    let mut results = vec![];
    for key in &config.keys {
        let account_data = sqlx::query_as::<_, AccountData>("SELECT * FROM account_data WHERE key == ?").bind(&key).fetch_one(connection).await.unwrap();
        let account = account_data.account.as_ref();

        let database_results = sqlx::query_as::<_, LoginRewardsItems>("SELECT * FROM login_rewards WHERE id == ?").bind(&account.id).fetch_all(connection).await.unwrap();
        results.extend(database_results);
    }
    results
}

pub async fn login_rewards(Extension(config): Extension<Config>, Extension(connection): Extension<Pool<Sqlite>>) -> impl IntoResponse {
    let results = get_login_rewards_db(&config, &connection).await;
    (StatusCode::OK, [(header::CONTENT_TYPE, "application/json; charset=utf-8")], Json(results))
}

pub async fn login_rewards_csv(Query(params): Query<HashMap<String, String>>, Extension(config): Extension<Config>, Extension(connection): Extension<Pool<Sqlite>>) -> impl IntoResponse {
    let results = get_login_rewards_db(&config, &connection).await;

    let delimiter = get_delimiter(&params);

    // https://github.com/BurntSushi/rust-csv/issues/151
    // csv does not like flatten, manually create the csv here
    let mut data: String = format!("id{d}display_name{d}last_update{d}wallet_laurel", d = delimiter);

    let mut keys_main: Vec<_> = vec![];
    if !results.is_empty() {
        let first = &results[0];
        keys_main = first.items.keys().collect();

        // not fantastic
        for key in &keys_main {
            // https://rust-lang.github.io/rust-clippy/master/index.html#format_push_string
            let _ = write!(data, "{d}{}", key, d = delimiter);
        }
    }

    for result in &results {
        // data that is guaranteed to be there
        let _ = write!(data, "\n{}{d}{}{d}{}{d}{}", result.id, result.display_name, result.last_update, result.wallet_laurel, d = delimiter);

        for key in &keys_main {
            if let Some(x) = result.items.get(key) {
                let _ = write!(data, "{d}{}", x, d = delimiter);
            }
        }
    }

    (StatusCode::OK, [(header::CONTENT_TYPE, "text/csv; charset=utf-8"), (header::CONTENT_DISPOSITION, "attachment; filename=login_rewards.csv")], data)
}

fn get_delimiter(params: &HashMap<String, String>) -> char {
    if let Some(custom_delimiter) = params.get("delimiter") {
        if let Some(character) = custom_delimiter.chars().next() {
            return character;
        }
    }
    // fallback to comma
    ','
}
