mod endpoints;

use crate::endpoints::{db_init, free_bag_slots, free_bag_slots_csv, get_account_data, get_free_bag_slots, get_login_rewards, login_rewards, login_rewards_csv};
use axum::{routing::get, Extension, Router};

use chrono::{Local, Timelike};
use gw2lib::rate_limit::BucketRateLimiter;
use serde::{Deserialize, Serialize};
use std::fmt::Display;
use std::time::Duration;
use std::{fs, net::SocketAddr, sync::Arc, thread};
use tokio::sync::Mutex;

#[tokio::main]
async fn main() {
    // read imputs from file, basically api keys

    let config = converter_get_config();

    let addr = SocketAddr::from(([127, 0, 0, 1], config.port));
    let connection = db_init(&config.database).await.unwrap();
    let rate_limiter = Arc::new(Mutex::new(BucketRateLimiter::default()));

    let cron_rate_limiter = Arc::clone(&rate_limiter);
    let cron_config = config.clone();
    let cron_connection = connection.clone();

    print_message("Version", env!("CARGO_PKG_VERSION"), None::<i8>);
    print_message("Listening on", addr, None::<i8>);
    print_message("Update every", config.update_interval, Some("minutes"));

    // these are for updating the cache
    tokio::spawn(async move {
        loop {
            // gets the bulk of teh raw data, once
            get_account_data(&cron_config, &cron_rate_limiter, &cron_connection).await;
            get_free_bag_slots(&cron_config, &cron_connection).await;
            get_login_rewards(&cron_config, &cron_rate_limiter, &cron_connection).await;

            print_message("Updated cache", "", None::<i8>);
            thread::sleep(Duration::from_secs((60 * config.update_interval) as u64));
        }
    });

    let app = Router::new()
        // Enko
        .route("/free_bag_slots", get(free_bag_slots))
        .route("/free_bag_slots/csv", get(free_bag_slots_csv))
        // Weaver
        .route("/login_rewards", get(login_rewards))
        .route("/login_rewards/csv", get(login_rewards_csv))
        // need to make the rate limiter config are available to everything
        .layer(Extension(rate_limiter))
        .layer(Extension(config))
        .layer(Extension(connection));

    axum::Server::bind(&addr).serve(app.into_make_service()).await.unwrap();
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Config {
    port: u16,
    update_interval: u16,
    database: String,
    keys: Vec<String>,
}
#[derive(Debug, Deserialize)]
struct ConfigRaw {
    port: Option<u16>,
    update_interval: Option<u16>,
    database: Option<String>,
    keys: Option<Vec<String>>,
}

fn converter_get_config() -> Config {
    let filename = "config.toml";

    let mut result = Config {
        // no idea why I chose this port
        port: 8062,
        update_interval: 15,
        database: String::from("database.db"),
        keys: vec![],
    };
    if let Ok(config_file) = fs::read_to_string(filename) {
        if let Ok(config) = toml::from_str::<ConfigRaw>(&*config_file) {
            print_message("Found", filename, Some("overriding defaults"));
            match config.port {
                None => {}
                Some(x) => result.port = x,
            }
            match config.update_interval {
                None => {}
                Some(x) => result.update_interval = x,
            }
            match config.database {
                None => {}
                Some(x) => result.database = x,
            }
            match config.keys {
                None => {}
                Some(x) => result.keys = x,
            }
        }
    } else {
        print_message("No", filename, Some("file, creating and using defaults"));
        if let Ok(config_string) = toml::to_string(&result) {
            fs::write(filename, config_string).unwrap();
        }
    }

    result
}

fn print_message<T: Display, U: Display>(text: &str, value: T, value2: Option<U>) {
    if let Some(val2) = value2 {
        println!("{} {} {} {}", get_current_timestamp(), text, value, val2);
    } else {
        println!("{} {} {}", get_current_timestamp(), text, value);
    }
}

fn get_current_timestamp() -> String {
    if let Some(time) = Local::now().with_nanosecond(0) {
        return time.to_string();
    }
    String::default()
}
